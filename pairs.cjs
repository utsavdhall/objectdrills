function pairs(testObject){
    let pairsArray=[];
    for(let keys in testObject){
        pairsArray.push([keys,testObject[keys]]);
    }
    return pairsArray;
}

module.exports=pairs;