function defaults(testObject,defaultProps){
    for(let keys in testObject){
        if(testObject[keys]==undefined){
            testObject[keys]=defaultProps[keys];
        }
    }
}

module.exports=defaults;