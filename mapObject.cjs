function mapObjects(testObject,cb){
    let obj={};
    for(let keys in testObject){
        obj[keys]=cb(testObject[keys]);
    }
    return obj;
}

module.exports = mapObjects;